from __future__ import annotations

from dataclasses import dataclass, field
from typing import TYPE_CHECKING, List

if TYPE_CHECKING:
    from .employees import Employee
    from .projects import Project


@dataclass
class Company:
    name: str
    employees: List[Employee] = field(default_factory=list, init=False)
    projects: List[Project] = field(default_factory=list, init=False)

    def add_employee(self, employee: Employee) -> None:
        if employee not in self.employees:
            self.employees.append(employee)
            employee.company = self

    def remove_employee(self, employee: Employee) -> None:
        self.employees.remove(employee)
        employee.company = None

    def find_employee(self, name: str) -> List[Employee]:
        return [employee for employee in self.employees if employee.name == name]

    def add_project(self, project: Project) -> None:
        if project not in self.projects:
            self.projects.append(project)

    def remove_project(self, project: Project) -> None:
        self.projects.remove(project)

    def report_labor(self) -> None:
        labor = sum([employee.calculate_pay(10) for employee in self.employees])
        print(f"{self.name} will expend ${labor} on employee wages per biweekly pay period")
        print("\nExpenses per project:")
        for project in self.projects:
            print(f"\t{project.name} costs ${project.calculate_labor(10)}")

    def pay_employees(self, days) -> None:
        for employee in self.employees:
            employee.pay(days)
