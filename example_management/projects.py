from __future__ import annotations

from dataclasses import dataclass, field
from typing import TYPE_CHECKING, List

if TYPE_CHECKING:
    from .employees import Employee


@dataclass
class Project:
    name: str
    assigned_employees: List[Employee] = field(default_factory=list, init=False)

    def assign_to(self, employee: Employee) -> None:
        if employee not in self.assigned_employees:
            self.assigned_employees.append(employee)
            if employee.project:
                employee.project.unassign(employee)
            employee.project = self

    def unassign(self, employee: Employee) -> None:
        if employee in self.assigned_employees:
            employee.project = None
            self.assigned_employees.remove(employee)

    def calculate_labor(self, days: int) -> float:
        labor = 0.0
        for employee in self.assigned_employees:
            labor += employee.calculate_pay(days)
        return labor
