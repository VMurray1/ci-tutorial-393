from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import TYPE_CHECKING, List

if TYPE_CHECKING:
    from .company import Company
    from .projects import Project


@dataclass
class Employee(ABC):
    name: str
    job: str
    age: int
    company: Company = field(default=None, init=False)
    project: Project = field(default=None, init=False)

    @abstractmethod
    def calculate_pay(self, days: int) -> float:
        return

    @abstractmethod
    def pay(self, days: int) -> float:
        return


@dataclass
class HourlyEmployee(Employee):
    wage: float
    weekly_hours_avg: int

    def calculate_pay(self, days: int) -> float:
        hours = self.weekly_hours_avg / 5 * days
        return round(hours * self.wage, 2)

    def pay(self, days: int) -> float:
        amt = self.calculate_pay(days)
        print(f"Paying {self.name} ${amt} for {days / 5 * self.weekly_hours_avg} hours at ${self.wage}/hr.")


@dataclass
class SalaryEmployee(Employee):
    salary: int

    def calculate_pay(self, days: int) -> float:
        daily_pay = self.salary / 52 / 5
        return round(days * daily_pay, 2)

    def pay(self, days: int) -> float:
        amt = self.calculate_pay(days)
        print(f"Paying {self.name} ${amt} for {days} days at ${self.salary}/yr.")
