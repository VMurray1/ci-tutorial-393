import io
import sys
from unittest import TestCase

from example_management import Company, Employee, Project


class MockEmployee(Employee):
    def calculate_pay(self, days: int):
        return days

    def pay(self, days: int):
        print(f"Paid {self.name} ${self.calculate_pay(days)}")


class MockProject(Project):
    def calculate_labor(self, days: int):
        return days


class CompanyTest(TestCase):
    def test_employees(self):
        # Create company and add employees
        company = Company("Foo Corporation")
        employees = [
            MockEmployee("Sally", "Manager", 45),
            MockEmployee("John", "Clerk", 25),
            MockEmployee("John", "Security", 30),
        ]
        for employee in employees:
            company.add_employee(employee)

        # Make sure company contains all employees
        self.assertEqual(company.employees, employees)

        # Test company.find_employee() method
        self.assertEqual(employees[0:1], company.find_employee("Sally"))
        self.assertEqual(employees[1:], company.find_employee("John"))

        # Test company.remove_employee() method
        self.assertEqual(company, employees[0].company)
        company.remove_employee(employees[0])
        self.assertTrue(employees[0] not in company.employees)
        self.assertEqual(None, employees[0].company)

    def test_projects(self):
        # Create company and add projects
        company = Company("FooBar Corporation")
        projects = [MockProject("Foo"), MockProject("Bar"), MockProject("Qux")]
        for proj in projects:
            company.add_project(proj)

        self.assertEqual(company.projects, projects)

        # Test to make sure you can't add duplicate projects
        company.add_project(projects[0])
        self.assertEqual(company.projects, projects)

        # Test remove projects
        company.remove_project(projects[1])
        self.assertEqual(company.projects, projects[::2])

    def test_pay(self):
        # Create company and add employees
        company = Company("Bar Corporation")
        employees = [
            MockEmployee("Sally", "Manager", 45),
            MockEmployee("John", "Clerk", 25),
            MockEmployee("John", "Security", 30),
        ]
        for employee in employees:
            company.add_employee(employee)

        # Redirect stdout to StringIO to capture console output
        captureStdout = io.StringIO()
        sys.stdout = captureStdout
        company.pay_employees(10)
        sys.stdout = sys.__stdout__

        # Compare results with expected output
        expectedOutput = "Paid Sally $10\nPaid John $10\nPaid John $10\n"
        self.assertEqual(captureStdout.getvalue(), expectedOutput)

    def test_cost(self):
        # Create company and add employees
        company = Company("FooBar Corporation")
        employees = [
            MockEmployee("Sally", "Manager", 45),
            MockEmployee("John", "Clerk", 25),
            MockEmployee("John", "Security", 30),
        ]
        for employee in employees:
            company.add_employee(employee)

        # Create and add projects
        projects = [MockProject("Foo"), MockProject("Bar"), MockProject("Qux")]
        for proj in projects:
            company.add_project(proj)

        # Redirect stdout to StringiO to capture console output
        captureStdout = io.StringIO()
        sys.stdout = captureStdout
        company.report_labor()
        sys.stdout = sys.__stdout__

        # Compare results with expected output
        expectedOutput = "\n".join(
            [
                "FooBar Corporation will expend $30 on employee wages per biweekly pay period",
                "",
                "Expenses per project:",
                "\tFoo costs $10",
                "\tBar costs $10",
                "\tQux costs $10",
                "",
            ]
        )
        self.assertEqual(captureStdout.getvalue(), expectedOutput)
