from unittest import TestCase

from example_management import Employee, Project


class MockEmployee(Employee):
    def calculate_pay(self, days: int):
        return days

    def pay(self, days: int):
        return


class TestProject(TestCase):
    def test_assign(self):
        project = Project("Foo")
        employees = [
            MockEmployee("Sally", "Manager", 45),
            MockEmployee("John", "Clerk", 25),
            MockEmployee("John", "Security", 30),
        ]

        project.assign_to(employees[0])
        self.assertEqual(employees[0].project, project)
        self.assertEqual(project.assigned_employees, employees[0:1])

        project.assign_to(employees[2])
        self.assertEqual(employees[2].project, project)
        self.assertEqual(project.assigned_employees, employees[::2])

        project.unassign(employees[0])
        self.assertEqual(employees[0].project, None)
        self.assertEqual(project.assigned_employees, employees[2:])

        barProject = Project("Bar")
        barProject.assign_to(employees[2])
        self.assertEqual(project.assigned_employees, [])
        self.assertEqual(barProject.assigned_employees, employees[2:])
        self.assertEqual(employees[2].project, barProject)

    def test_calculate_labor(self):
        project = Project("Foo")
        employees = [
            MockEmployee("Sally", "Manager", 45),
            MockEmployee("John", "Clerk", 25),
            MockEmployee("John", "Security", 30),
        ]
        for employee in employees:
            project.assign_to(employee)

        self.assertEqual(project.calculate_labor(10), 30)
