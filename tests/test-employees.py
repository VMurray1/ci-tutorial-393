import io
import sys
from unittest import TestCase

from example_management import HourlyEmployee, SalaryEmployee


class TestHourlyEmployee(TestCase):
    def test_calculate_pay(self):
        employee = HourlyEmployee("Sally", "Clerk", 25, 10, 40)
        self.assertEqual(employee.calculate_pay(10), 800)
        self.assertEqual(employee.calculate_pay(5), 400)

        employee.wage = 15.0
        self.assertEqual(employee.calculate_pay(10), 1200)
        self.assertEqual(employee.calculate_pay(5), 600)

    def test_pay(self):
        employee = HourlyEmployee("Sally", "Clerk", 25, 10, 40)

        captureStdout = io.StringIO()
        sys.stdout = captureStdout
        employee.pay(10)
        sys.stdout = sys.__stdout__

        self.assertEqual(captureStdout.getvalue(), "Paying Sally $800.0 for 80.0 hours at $10/hr.\n")

        captureStdout = io.StringIO()
        sys.stdout = captureStdout
        employee.pay(5)
        sys.stdout = sys.__stdout__

        self.assertEqual(captureStdout.getvalue(), "Paying Sally $400.0 for 40.0 hours at $10/hr.\n")

        employee.wage = 15

        captureStdout = io.StringIO()
        sys.stdout = captureStdout
        employee.pay(10)
        sys.stdout = sys.__stdout__

        self.assertEqual(captureStdout.getvalue(), "Paying Sally $1200.0 for 80.0 hours at $15/hr.\n")

        captureStdout = io.StringIO()
        sys.stdout = captureStdout
        employee.pay(5)
        sys.stdout = sys.__stdout__

        self.assertEqual(captureStdout.getvalue(), "Paying Sally $600.0 for 40.0 hours at $15/hr.\n")


class TestSalaryEmployee(TestCase):
    def test_calculate_pay(self):
        employee = SalaryEmployee("Sally", "Manager", 35, 80000)
        self.assertEqual(employee.calculate_pay(10), 3076.92)
        self.assertEqual(employee.calculate_pay(5), 1538.46)

        employee.salary = 100000
        self.assertEqual(employee.calculate_pay(10), 3846.15)
        self.assertEqual(employee.calculate_pay(5), 1923.08)

    def test_pay(self):
        employee = SalaryEmployee("Sally", "Manager", 35, 80000)

        captureStdout = io.StringIO()
        sys.stdout = captureStdout
        employee.pay(10)
        sys.stdout = sys.__stdout__

        self.assertEqual(captureStdout.getvalue(), "Paying Sally $3076.92 for 10 days at $80000/yr.\n")

        captureStdout = io.StringIO()
        sys.stdout = captureStdout
        employee.pay(5)
        sys.stdout = sys.__stdout__

        self.assertEqual(captureStdout.getvalue(), "Paying Sally $1538.46 for 5 days at $80000/yr.\n")

        employee.salary = 100000

        captureStdout = io.StringIO()
        sys.stdout = captureStdout
        employee.pay(10)
        sys.stdout = sys.__stdout__

        self.assertEqual(captureStdout.getvalue(), "Paying Sally $3846.15 for 10 days at $100000/yr.\n")

        captureStdout = io.StringIO()
        sys.stdout = captureStdout
        employee.pay(5)
        sys.stdout = sys.__stdout__

        self.assertEqual(captureStdout.getvalue(), "Paying Sally $1923.08 for 5 days at $100000/yr.\n")
