# Automating Python package builds, unit tests, and deployment using GitLab CI/CD

## Introduction

In the process of developing computer software, as the complexity of the software product rises it quickly develops a significant development overhead for building, testing, and deploying the software product. These tasks are often repetitive and tedious, so it is often beneficial to automate them in order to free up time for the development team to continue improving the product. In the case of Python, there are a number of tools available for doing this. Building a Python library is typically done using `setuptools` by creating a `setup.py` file, and the Python standard library contains the module `unittest`, which as the name implies, is an excellent way of implementing basic software unittests for Python modules. These instructions will first show the user how to construct a basic Python library and commit it as a GitLab repository. Then they will use this library as a basis to develop a simple `setup.py` script, implement basic unit tests, and then develop a `gitlab-ci.yml` file to automate building, testing, and deploying this library. In addition, the completed GitLab repository will be available publically for reference.

## Warning

Depending on the security standards of the user's organization, it may or may not be permissible to place source code on GitLab.com. Many organizations have an internal GitLab instance, and NIST in particular has an internal instance of GitLab available for developer use. Contact a supervisor if you have any questions or concerns regarding the use of GitLab.

## Technical Background

Although not strictly necessary for this tutorial, it is assumed that users are familiar with the Python programming language and using Git for software version control. Information on learning these technologies is ubiquitous online. Also requires basic knowledge of using the terminal.

## Requirements

- [Python 3.7](https://www.python.org) or higher
- A text editor or IDE suitable for software development
- [Git](https://git-scm.com/)
- GitLab access using either a [gitlab.com](https://www.gitlab.com) account or an internal instance of GitLab (See [Warning](#Warning))

## Setup

Download the example library from [GitLab](https://gitlab.com/VMurray1/ci-tutorial-393/uploads/1d46919daca98d3a3ea55108006eeb51/example_management.zip) and unzip the archive into an empty directory. The formerly empty folder that now contains `example_management` is henceforth referred to as the "project directory" or "project folder."

## Part 1: Implementing Unit Tests and the build script

1. Create a new directory within the project folder called `tests` and create 3 files within it, `test-company.py`, `test-employees.py`, and `test-project.py`. Each of these files will be used to implement unit tests for their corresponding Python module. The file tree should now appear as follows

   ![Step 1 File Tree](assets/Step1FileTree.png)

2. Open `test-company.py` in any text editor. In order to implement unit tests for each of the methods of the `Company` class, we will obviously need to import the `unittest` library and `company` module. In addition, `company` requires `employee`, and `project` objects as arguments for some methods, so import them as well, like so:

    ```python
    from unittest import TestCase

    from example_management import Company, Employee, Project
    ```

3. In addition, in order to test *only* the `Company` class, we will need to implement mock `Employee` and `Project` classes, so that this test does not depend on `Employee` and `Project` being implemented correctly. This requires defining a new `MockEmployee` and `MockCompany` class that override the methods of their corresponding base classes for testing purposes.

    ```python
    class MockEmployee(Employee):
        def calculate_pay(self, days: int):
            return days

       def pay(self, days: int):
           print(f"Paid {self.name} ${self.calculate_pay(days)}")


   class MockProject(Project):
       def calculate_labor(self, days: int):
           return days
    ```

4. Now we can begin to actually implement test cases. In order to implement unit tests using `unittest` you need to define a class (lets call it `TestCompany`) that inherits from `unittest.TestCase`, and add methods to this new class that generate input data for each of the functionalities of the `Company` class and then compare the results with the results we *should* receive as long as Company is implemented without bugs. Thus a `TestCase`, with one method that tests the employee tracking functionality of `Company` should look something like this:

    ```python
    class CompanyTest(TestCase):
       def test_employees(self):
           # Create company and add employees
           company = Company("Foo Corporation")
           employees = [
               MockEmployee("Sally", "Manager", 45),
               MockEmployee("John", "Clerk", 25),
               MockEmployee("John", "Security", 30),
           ]
           for employee in employees:
               company.add_employee(employee)

           # Make sure company matches expectation by comparing string representation
           # Not recommended in actual projects
           self.assertEqual(
               str(company),
               "Company(name='Foo Corporation', employees=[MockEmployee(name='Sally', job='Manager', age=45, company=..., projects=[]), MockEmployee(name='John', job='Clerk', age=25, company=..., projects=[]), MockEmployee(name='John', job='Security', age=30, company=..., projects=[])], projects=[])",
           )

           # Test company.find_employee() method
           self.assertEqual(employees[0:1], company.find_employee("Sally"))
           self.assertEqual(employees[1:], company.find_employee("John"))

           # Test company.remove_employee() method
           self.assertEqual(company, employees[0].company)
           company.remove_employee(employees[0])
           self.assertTrue(employees[0] not in company.employees)
           self.assertEqual(None, employees[0].company)
    ```

    This method creates a `Company` and a set of `MockEmployee`s and then uses them to test each of the `Employee` tracking methods implemented by `Company`

5. Repeat this process to create a method within the `CompanyTest` class to test the project tracking functionality of `Company`. This method should create a `Company` and a set of `Project`s and then test the `Company.add_project()` and `Company.remove_project()` methods against their expected effect. Such a test would appear similar to the following.

    ```python
    def test_projects(self):
        # Create company and add projects
        company = Company("FooBar Corporation")
        projects = [MockProject("Foo"), MockProject("Bar"), MockProject("Qux")]
        for proj in projects:
            company.add_project(proj)

        self.assertEqual(company.projects, projects)

        # Test to make sure you can't add duplicate projects
        company.add_project(projects[0])
        self.assertEqual(company.projects, projects)

        # Test remove projects
        company.remove_project(projects[1])
        self.assertEqual(company.projects, projects[::2])
    ```

6. Next, we need to add methods to the `CompanyTest` class that test the `Company.report_labor()` and `Company.pay_employees()` methods. However, these two methods function a bit differently than the functionality we have already tested. The `report_labor()` and `pay_employees()` method print their results to standard output instead of returning a value. In order to compare the result of these methods against the result we would expect them to give we need some way of capturing standard output to get a Python string object. In order to do this, we need the `io` and `sys` classes, so add them to the list of imports at the top of the file.

    ```python
    import sys
    import io
    ```

    Now, we can capture the text a method prints to standard output by redirecting standard output to a `io.StringIO` object and then executing the method like so

    ```python
    captureStdout = io.StringIO()
    sys.stdout = captureStdout
    # Method to test here
    sys.stdout = sys.__stdout__

    # Get results by running:
    captureStdout.getvalue()
    ```

    With this capability, implement two methods in the `CompanyTest` class to test the `Company.report_labor()` and `Company.pay_employees()` methods. The final `test-company.py` file should look approximately as follows:

    ```python
    import io
    import sys
    from unittest import TestCase

    from example_management import Company, Employee, Project


    class MockEmployee(Employee):
        def calculate_pay(self, days: int):
            return days

        def pay(self, days: int):
            print(f"Paid {self.name} ${self.calculate_pay(days)}")


    class MockProject(Project):
        def calculate_labor(self, days: int):
            return days


    class CompanyTest(TestCase):
        def test_employees(self):
            # Create company and add employees
            company = Company("Foo Corporation")
            employees = [
                MockEmployee("Sally", "Manager", 45),
                MockEmployee("John", "Clerk", 25),
                MockEmployee("John", "Security", 30),
            ]
            for employee in employees:
                company.add_employee(employee)

            # Make sure company contains all employees
            self.assertEqual(company.employees, employees)

            # Test company.find_employee() method
            self.assertEqual(employees[0:1], company.find_employee("Sally"))
            self.assertEqual(employees[1:], company.find_employee("John"))

            # Test company.remove_employee() method
            self.assertEqual(company, employees[0].company)
            company.remove_employee(employees[0])
            self.assertTrue(employees[0] not in company.employees)
            self.assertEqual(None, employees[0].company)

        def test_projects(self):
            # Create company and add projects
            company = Company("FooBar Corporation")
            projects = [MockProject("Foo"), MockProject("Bar"), MockProject("Qux")]
            for proj in projects:
                company.add_project(proj)

            self.assertEqual(company.projects, projects)

            # Test to make sure you can't add duplicate projects
            company.add_project(projects[0])
            self.assertEqual(company.projects, projects)

            # Test remove projects
            company.remove_project(projects[1])
            self.assertEqual(company.projects, projects[::2])

        def test_pay(self):
            # Create company and add employees
            company = Company("Bar Corporation")
            employees = [
                MockEmployee("Sally", "Manager", 45),
                MockEmployee("John", "Clerk", 25),
                MockEmployee("John", "Security", 30),
            ]
            for employee in employees:
                company.add_employee(employee)

            # Redirect stdout to StringIO to capture console output
            captureStdout = io.StringIO()
            sys.stdout = captureStdout
            company.pay_employees(10)
            sys.stdout = sys.__stdout__

            # Compare results with expected output
            expectedOutput = "Paid Sally $10\nPaid John $10\nPaid John $10\n"
            self.assertEqual(captureStdout.getvalue(), expectedOutput)

        def test_cost(self):
            # Create company and add employees
            company = Company("FooBar Corporation")
            employees = [
                MockEmployee("Sally", "Manager", 45),
                MockEmployee("John", "Clerk", 25),
                MockEmployee("John", "Security", 30),
            ]
            for employee in employees:
                company.add_employee(employee)

            # Create and add projects
            projects = [MockProject("Foo"), MockProject("Bar"), MockProject("Qux")]
            for proj in projects:
                company.add_project(proj)

            # Redirect stdout to StringiO to capture console output
            captureStdout = io.StringIO()
            sys.stdout = captureStdout
            company.report_labor()
            sys.stdout = sys.__stdout__

            # Compare results with expected output
            expectedOutput = "\n".join(
                [
                    "FooBar Corporation will expend $30 on employee wages per biweekly pay period",
                    "",
                    "Expenses per project:",
                    "\tFoo costs $10",
                    "\tBar costs $10",
                    "\tQux costs $10",
                    "",
                ]
            )
            self.assertEqual(captureStdout.getvalue(), expectedOutput)
    ```

7. Repeat steps 2-6 to implement tests for the `Project` class in `test-project.py` the resulting `TestCase` should look roughly like this:

    ```python
    from unittest import TestCase

    from example_management import Employee, Project


    class MockEmployee(Employee):
        def calculate_pay(self, days: int):
            return days

        def pay(self, days: int):
            return


    class TestProject(TestCase):
        def test_assign(self):
            project = Project("Foo")
            employees = [
                MockEmployee("Sally", "Manager", 45),
                MockEmployee("John", "Clerk", 25),
                MockEmployee("John", "Security", 30),
            ]

            project.assign_to(employees[0])
            self.assertEqual(employees[0].project, project)
            self.assertEqual(project.assigned_employees, employees[0:1])

            project.assign_to(employees[2])
            self.assertEqual(employees[2].project, project)
            self.assertEqual(project.assigned_employees, employees[::2])

            project.unassign(employees[0])
            self.assertEqual(employees[0].project, None)
            self.assertEqual(project.assigned_employees, employees[2:])

            barProject = Project("Bar")
            barProject.assign_to(employees[2])
            self.assertEqual(project.assigned_employees, [])
            self.assertEqual(barProject.assigned_employees, employees[2:])
            self.assertEqual(employees[2].project, barProject)

        def test_calculate_labor(self):
            project = Project("Foo")
            employees = [
                MockEmployee("Sally", "Manager", 45),
                MockEmployee("John", "Clerk", 25),
                MockEmployee("John", "Security", 30),
            ]
            for employee in employees:
                project.assign_to(employee)

            self.assertEqual(project.calculate_labor(10), 30)
    ```

8. Repeat step 2 and step 4-6 to implement tests for the `HourlyEmployee` and `SalaryEmployee` classes of `employees` in the `test-employees.py` file. Separate these tests into a `TestHourlyEmployee` and a `TestSalaryEmployee` class for clarity like so:

    ```python
    import io
    import sys
    from unittest import TestCase

    from example_management import HourlyEmployee, SalaryEmployee


    class TestHourlyEmployee(TestCase):
        def test_calculate_pay(self):
            employee = HourlyEmployee("Sally", "Clerk", 25, 10, 40)
            self.assertEqual(employee.calculate_pay(10), 800)
            self.assertEqual(employee.calculate_pay(5), 400)

            employee.wage = 15.0
            self.assertEqual(employee.calculate_pay(10), 1200)
            self.assertEqual(employee.calculate_pay(5), 600)

        def test_pay(self):
            employee = HourlyEmployee("Sally", "Clerk", 25, 10, 40)

            captureStdout = io.StringIO()
            sys.stdout = captureStdout
            employee.pay(10)
            sys.stdout = sys.__stdout__

            self.assertEqual(captureStdout.getvalue(), "Paying Sally $800.0 for 80.0 hours at $10/hr.\n")

            captureStdout = io.StringIO()
            sys.stdout = captureStdout
            employee.pay(5)
            sys.stdout = sys.__stdout__

            self.assertEqual(captureStdout.getvalue(), "Paying Sally $400.0 for 40.0 hours at $10/hr.\n")

            employee.wage = 15

            captureStdout = io.StringIO()
            sys.stdout = captureStdout
            employee.pay(10)
            sys.stdout = sys.__stdout__

            self.assertEqual(captureStdout.getvalue(), "Paying Sally $1200.0 for 80.0 hours at $15/hr.\n")

            captureStdout = io.StringIO()
            sys.stdout = captureStdout
            employee.pay(5)
            sys.stdout = sys.__stdout__

            self.assertEqual(captureStdout.getvalue(), "Paying Sally $600.0 for 40.0 hours at $15/hr.\n")


    class TestSalaryEmployee(TestCase):
        def test_calculate_pay(self):
            employee = SalaryEmployee("Sally", "Manager", 35, 80000)
            self.assertEqual(employee.calculate_pay(10), 3076.92)
            self.assertEqual(employee.calculate_pay(5), 1538.46)

            employee.salary = 100000
            self.assertEqual(employee.calculate_pay(10), 3846.15)
            self.assertEqual(employee.calculate_pay(5), 1923.08)

        def test_pay(self):
            employee = SalaryEmployee("Sally", "Manager", 35, 80000)

            captureStdout = io.StringIO()
            sys.stdout = captureStdout
            employee.pay(10)
            sys.stdout = sys.__stdout__

            self.assertEqual(captureStdout.getvalue(), "Paying Sally $3076.92 for 10 days at $80000/yr.\n")

            captureStdout = io.StringIO()
            sys.stdout = captureStdout
            employee.pay(5)
            sys.stdout = sys.__stdout__

            self.assertEqual(captureStdout.getvalue(), "Paying Sally $1538.46 for 5 days at $80000/yr.\n")

            employee.salary = 100000

            captureStdout = io.StringIO()
            sys.stdout = captureStdout
            employee.pay(10)
            sys.stdout = sys.__stdout__

            self.assertEqual(captureStdout.getvalue(), "Paying Sally $3846.15 for 10 days at $100000/yr.\n")

            captureStdout = io.StringIO()
            sys.stdout = captureStdout
            employee.pay(5)
            sys.stdout = sys.__stdout__

            self.assertEqual(captureStdout.getvalue(), "Paying Sally $1923.08 for 5 days at $100000/yr.\n")
    ```

9. In the project directory create a new file named `setup.py`. This is the industry standard place to implement `setuptools` in order to build/install the Python library. This setup.py script is the script that will be executed whenever installing this library using `pip` and is also used to build wheels for distribution. Copy the following into `setup.py`.

    ```python
    from setuptools import setup

    setup(
        name="example-management",
        author="Your name here",
        description="Example Library for CI/CD tutorial",
        packages=["example_management"],
    )
    ```

    The most important piece here is the `packages` argument of the `setup` method. This argument tells `setuptools` where to find the actual implementation of the Python package.

10. Now that we have the `setup.py` file complete, we can test to make sure everything works on the local machine. In the terminal, navigate to the project directory, and install the package using `pip install .`. You may get one or more `DEPRECATION` warnings and/or a `pip` version warning, these can be ignored. Then, change directories into the tests directory and run the tests using the command `python -m unittest run {TEST MODULE HERE}`, substituting in the name of the test module (the filename without the `.py`) to run. Run all three test modules. The output of all of this should look like this:

    ```sh
    projectDirectory>pip install .
    Defaulting to user installation because normal site-packages is not writeable
    Processing projectDirectory

    Building wheels for collected packages: example-management
      Building wheel for example-management (setup.py) ... done
      Created wheel for example-management: filename=example_management-0.0.0-py3-none-any.whl size=3283 sha256=c890b06bbcf1e687a280bf9c30663a4d868fac0b297480ae9051af67e915dad4
      Stored in directory: tempDir
    Successfully built example-management
    Installing collected packages: example-management
    Successfully installed example-management-0.0.0

    projectDirectory> cd tests

    projectDirectory\tests>python -m unittest run test-company
    .....
    ----------------------------------------------------------------------
    Ran 5 tests in 0.008s

    OK

    projectDirectory\tests>python -m unittest run test-project
    ...
    ----------------------------------------------------------------------
    Ran 3 tests in 0.008s

    OK

    projectDirectory\tests>python -m unittest run test-employees
    .....
    ----------------------------------------------------------------------
    Ran 5 tests in 0.008s

    OK
    ```

## Part 2: Adding the library to a GitLab repository and automating workflow with GitLab CI/CD

1. On [GitLab.com](https://www.gitlab.com) or on your organizations internal instance of GitLab, log in and create a new blank project by pressing the `New project` button on the homepage, and then pressing the large `Create blank project` button. Fill in the Project name, and **UNCHECK the checkbox that says `Initialize repository with a readme`** and press `Create project`.

   ![GitLab blank project creation screen](assets/Part2Step1BlankProject.png)

2. Create a blank `readme.md` file in the project directory. Then create a .gitignore file in a project directory and add the text `__pycache__` to it. This will cause `git` to ignore any Python bytecode cache files produced when running code locally. The file tree should now look like this

    ![Project File Tree](assets/Part2Step2FileTree.png)

3. Switch back to the project repository view in GitLab. If you have never used Git on this machine, or you registered your GitLab account under a different email than your account on other Git platforms (i.e. GitHub or BitBucket) run the commands listed under `Git global setup`.

    ![Git setup commands](assets/Part2Step3GitCMDs.png)

4. In the terminal, change directories to the project directory and run the commands listed under the `Push an existing folder` section to initialize a repository on your local machine, commit files, and then push it to the GitLab repository.

    ![Git setup commands](assets/Part2Step4GitCMDs.png)

    ```sh
    projectDirectory> git init--initial-branch=main
    Initialized empty Git repository in projectDirectory/.git/

    projectDirectory> git remote add origin repositoryURL.git

    projectDirectory> git add .

    projectDirectory>git commit -m "Initial commit"
    [main (root-commit) 01dabc8] Initial commit
     10 files changed, 403 insertions(+)
     create mode 100644 .gitignore
     create mode 100644 example_management/__init__.py
     create mode 100644 example_management/company.py
     create mode 100644 example_management/employees.py
     create mode 100644 example_management/projects.py
     create mode 100644 readme.md
     create mode 100644 setup.py
     create mode 100644 tests/test-company.py
     create mode 100644 tests/test-employees.py
     create mode 100644 tests/test-project.py

    projectDirectory> git push -u origin main
    Enumerating objects: 14, done.
    Counting objects: 100% (14/14), done.
    Delta compression using up to 12 threads
    Compressing objects: 100% (12/12), done.
    Writing objects: 100% (14/14), 3.78 KiB | 1.89 MiB/s, done.
    Total 14 (delta 2), reused 0 (delta 0), pack-reused 0
    To repositoryURL.git
     * [new branch]      main -> main
    Branch 'main' set up to track remote branch 'main' from 'origin'.
    ```

    Refresh the GitLab window, it should now appear like this:

    ![GitLab view after initial commit](assets/Part2Step4InitialCommitView.png)

    This project is now hosted as a git repository on GitLab.

5. In order to implement the GitLab CI/CD workflow create a `.gitlab-ci.yml` file in the project directory. Open this file in a text editor. The workflow will consist of three stages: build, test, and deploy, so define these stages in the `yml` file like so:

    ```yaml
    stages:
      - build
      - test
      - deploy
    ```

6. All of the stages in this pipeline are going to require Python 3.7 or later, and all jobs except for the build job will have to install the python library, so add a default job that calls the python 3.7 image and creates a `before_script` that `pip` installs the wheel in `dist/`. This is the directory that the build step will output the built Python wheel to.

    ```yaml
    default:
      image: python:3.7
      before_script:
        - pip install dist/*.whl
    ```

7. Now we can create the build step. The build step is going to have to execute the `setup.py` file to build the wheel, and the wheel is obviously not available at the start of this job so we have to override the `before_script` from the default job with something else. In this case the easiest way to do so is just run `python -V`. In addition, specify an artifact to tell GitLab CI/CD that this job should produce files within dist/ and that GitLab needs to pass these files on to subsequent jobs.

    ```yaml
    build:
      stage: build
      before_script:
        - python -V
      script:
        - python setup.py bdist_wheel
      artifacts:
        paths:
          - dist/

8. Next come the unit tests. Create jobs for each of the 3 unit test modules that run `python -m unittest run tests.{module_name here}` like so

    ```yaml
    test-company:
      stage: test
      script:
        - python -m unittest run tests.test-company

    test-project:
      stage: test
      script:
        - python -m unittest run tests.test-project

    test-employees:
      stage: test
      script:
        - python -m unittest run tests.test-employees
    ```

9. Finally, create the deploy job to register the wheel created in the build step to the GitLab package registry using `twine`.

    ```yaml
    deploy:
      stage: deploy
      before_script:
        - pip install twine
      script:
        - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
    ```

10. Now, add the file to the repository, commit, and push it to GitLab.

    ```sh
    projectDirectory> git add .

    projectDirectory> git commit -m "Add .gitlab-ci.yml"
    [main dfcbc72] Add .gitlab-ci.yml
     1 file changed, 41 insertions(+)
     create mode 100644 .gitlab-ci.yml

    projectDirectory> git push
    Enumerating objects: 4, done.
    Counting objects: 100% (4/4), done.
    Delta compression using up to 12 threads
    Compressing objects: 100% (3/3), done.
    Writing objects: 100% (3/3), 624 bytes | 624.00 KiB/s, done.
    Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
    To projectURL.git
       01dabc8..dfcbc72  main -> main
    ```

11. Switch back to the GitLab window and click on the CI/CD button in the sidebar on the left. If you are using GitLab.com, you may be asked to use a credit card to verify in order to be able to use the public GitLab runners to execute your pipeline. Verify with a credit card and then press the run `pipeline button`, and then hit the `run pipeline` button once again.

12. You should now see the following in the pipelines view window

    ![Pipelines view with pipeline](assets/Part2Step12PipelinesView.png)

    Click on the green `passed` box to view the pipeline. You should now see the pipeline along with a all of the jobs executed by this pipeline, broken down into stages.

    ![Pipeline Jobs](assets/Part2Step12Pipeline.png)

    Click on any of the jobs on this screen to view the logs for the job.

    ![Job log](assets/Part2Step12Log.png)

    In the future, as development continues, you can immediately see if a change that is committed to the repository causes one or more of the tests to break. This immediately notifies shows the developer that some breaking change has occurred, and then they can view the logs of the failing test to determine what the issue is when debugging the error.

13. In the sidebar on the left, click on the packages and registries button. This should display the package registry screen like shown.

    ![Package Registry](assets/Part2Step13PackageRegistry.png)

    Click on the example-management package. This view allows you to download the built wheel from anywhere by either clicking on the wheel file at the bottom of the screen, or registering a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) (outside the scope of these instructions) to download the package using pip using the command highlighted below.

    ![Package view](assets/Part2Step13Package.png)

This instruction set, completed, is [available on GitLab](https://gitlab.com/VMurray1/ci-tutorial-393)
